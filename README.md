﻿About
=====
monitors security metrics: monitors all parameters associated to each metric (e.g., age of reports, availability of repository, and responsiveness of scanners), sends monitoring events to [event-hub](https://bitbucket.org/specs-team/specs-monitoring-eventhub).


# Installation #

SVA core repository [SVA_core](https://bitbucket.org/specs-team/specs-mechanism-enforcement-sva_core), should be in the same dir level as this repository


Switch to root user:

```
#!bash

su root
```

Create a virtualenv with:

```
#!bash

virtualenv /path/to/env
source /path/to/env/bin/activate

```

Install requirements:


```
#!bash

pip install -r /path/to/specs_monitoring_sva/requirements.txt
```


# Usage #

Switch to root user:

```
#!bash

su root
```


Run enforcement with three arguments (scanning_frequency, list_update_frequency, up_report_frequency) in seconds:


```
#!bash

python /path/to/specs_monitoring_sva/src/monitoring.py run_monitoring 3600 3600 3600
```

You can also run each measurements separately:
### Penetration scanners availability ###
Checks availability of installed scanners with penetration testing
```
#!bash

TODO
```


### Repository availability ###
Checks for current repository availability

```
#!bash

python /path/to/specs_monitoring_sva/src/monitoring.py invoke_msr6
```

### List availbaility ###
Checks if vulnerability list is available
```
#!bash

python /path/to/specs_monitoring_sva/src/monitoring.py invoke_msr7
```

### Scanners availability ###
Checks availbility of installed scanner(openSCAP)

```
#!bash

python /path/to/specs_monitoring_sva/src/monitoring.py invoke_msr8
```

### Scan report availability ###
Checks if scan report is available

```
#!bash

python /path/to/specs_monitoring_sva/src/monitoring.py invoke_msr9
```

### Upgrade report availability ###
Checks if upgrade report is available

```
#!bash

python /path/to/specs_monitoring_sva/src/monitoring.py invoke_msr10
```


# Notice #

This product includes software developed at "XLAB d.o.o, Slovenia", as part of the "SPECS - Secure Provisioning of Cloud Services based on SLA Management" research project (an EC FP7-ICT Grant, agreement 610795).
* http://www.specs-project.eu/
* http://www.xlab.si/

Developers:

```
Aljaž Košir, aljaz.kosir@xlab.si
```

Copyright:

```
Copyright 2013-2015, XLAB d.o.o, Slovenia
    http://www.xlab.si

E2EE client is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License, version 3,
as published by the Free Software Foundation.

E2EE client is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```
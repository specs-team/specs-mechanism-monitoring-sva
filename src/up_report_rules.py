import os
from threading import Thread
from utils import get_file_age, run_in_periods, send_event_report, logger
from specs_sva_core import settings
from specs_sva_core.models import Settings
from constants import EVENT
import constants


class UpgradeReportRules:
    def __init__(self):
        self.up_report = settings.UP_REPORT
        self.scan_report = settings.SCAN_REPORT
        self.component_id = Settings.get().component_id
        self.sla_id = Settings.get().sla_id

    def run(self, config):
        threads = [Thread(target=self.get_scan_report_availability,
                          args=(config.scan_report_availability_frequency, config.availability_delay,)),
                   Thread(target=self.get_up_report_availability,
                          args=(config.up_report_availability_frequency, config.availability_delay,)),
                   Thread(target=self.get_up_report_age,
                          args=(config.up_report_age_frequency, config.age_delay,))
                   ]
        for thread in threads:
            thread.daemon = True
            thread.start()

    @run_in_periods
    def get_up_report_age(self, up_report_age_frequency, delay=0, event_type=EVENT):
        if up_report_age_frequency > 0:
            return self._send_up_report_age(event_type)
        return False

    @run_in_periods
    def get_scan_report_availability(self, scan_report_availability_frequency, delay=0, event_type=EVENT):
        file_available = os.path.isfile(self.scan_report)
        logger.info("Scan report available: %s" % file_available)
        return self.send_report(event_type, constants.SCAN_REPORT_AVAILABILITY, file_available)

    @run_in_periods
    def get_up_report_availability(self, up_report_availability_frequency, delay=0, event_type=EVENT):
        file_available = os.path.isfile(self.up_report)
        logger.info("Up report available: %s" % file_available)
        return self.send_report(event_type, constants.UP_REPORT_AVAILABILITY, file_available)

    def _send_up_report_age(self, event_type):
        if os.path.isfile(self.up_report):
            age = get_file_age(self.up_report)
            logger.info("Up report age: %s" % age)
            return self.send_report(event_type, constants.UP_REPORT_AGE, age)
        return False

    def send_report(self, event_type, type, value):
        return send_event_report(self.component_id, constants.UP_REPORT,
                                 [event_type, 'sla_id:%s' % self.sla_id, constants.UP_REPORT_METRIC], type, value)

import os
from threading import Thread
from utils import get_file_age, run_in_periods, send_event_report, logger
import subprocess
from specs_sva_core import settings
from specs_sva_core.models import Settings
from constants import EVENT
import constants


class BasicScanRules:
    def __init__(self):
        self.scan_report = settings.SCAN_REPORT
        self.vulnerability_list = settings.VULNERABILITY_LIST
        self.component_id = Settings.get().component_id
        self.sla_id = Settings.get().sla_id

    def run(self, config):
        threads = [Thread(target=self.get_scanner_availability,
                          args=(config.scanner_availability_frequency, config.availability_delay,)),
                   Thread(target=self.get_list_availability,
                          args=(config.list_availability_frequency, config.availability_delay,)),
                   Thread(target=self.get_report_basic_age,
                          args=(config.report_basic_age_frequency, config.age_delay,)),
                   ]
        for thread in threads:
            thread.daemon = True
            thread.start()

    @run_in_periods
    def get_report_basic_age(self, report_basic_age_frequency, delay=0, event_type=EVENT):
        if report_basic_age_frequency > 0:
            return self._send_basic_age(event_type)
        return False

    @run_in_periods
    def get_list_availability(self, list_availability_frequency, delay=0, event_type=EVENT):
        return self._send_list_availability(event_type)

    @run_in_periods
    def get_scanner_availability(self, scanner_availability_frequency, delay=0, event_type=EVENT):
        return self._send_scanner_availability(event_type)

    def _send_basic_age(self, event_type):
        if os.path.isfile(self.scan_report):
            age = get_file_age(self.scan_report)
            logger.info("Basic scan age: %s" % age)
            return self.send_report(event_type, constants.BASIC_AGE, age)
        return False

    def _send_list_availability(self, event_type):
        file_available = os.path.isfile(self.vulnerability_list)
        logger.info("Vulnerability list available: %s" % file_available)
        return self.send_report(event_type, constants.LIST_AVAILABILITY, file_available)

    def _send_scanner_availability(self, event_type):
        devnull = open(os.devnull, 'w')
        os.chdir(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..'))
        command = 'oscap oval eval --result files/results.xml files/oval.xml && rm files/results.xml'
        status_code = subprocess.call([command], shell=True, stderr=devnull, stdout=devnull)
        working = status_code == 0

        logger.info("Scanner availability: %s" % working)
        return self.send_report(event_type, constants.SCANNER_AVAILABILITY, working)

    def send_report(self, event_type, type, value):
        return send_event_report(self.component_id, constants.BASIC_SCAN,
                                 [event_type, 'sla_id:%s' % self.sla_id, constants.BASIC_SCAN_METRIC], type, value)

# Basic scan constants
BASIC_SCAN_METRIC = 'basic_scan_m13'
BASIC_SCAN = 'basic_scan'
BASIC_AGE = 'report_basic_age_sva_msr1'
LIST_AVAILABILITY = 'list_availability_sva_msr7'
SCANNER_AVAILABILITY = 'scanner_availability_sva_msr8'

# List update constants
LIST_UPDATE_METRIC = 'list_update_m14'
LIST_UPDATE = 'list_update'
LIST_AGE = 'list_age_sva_msr2'
REPOSITORY_AVAILABILITY = 'repository_availability_sva_msr6'

# Up report constants
UP_REPORT_METRIC = 'up_report_m23'
UP_REPORT = 'up_report'
SCAN_REPORT_AVAILABILITY = 'scan_report_availability_sva_msr9'
UP_REPORT_AVAILABILITY = 'up_report_availability_sva_msr10'
UP_REPORT_AGE = 'up_report_age_sva_msr4'

REMEDIATION_EVENT = 'event_type_remediation_event'
EVENT = 'event_type_event'

import time
import os
import sys
import logging
import threading
import datetime
import json
import pytz
from constants import EVENT
from specs_sva_core.models import Settings
from specs_sva_core import settings
from specs_sva_core.utils import send_post

SVA_MONITORING_ADAPTER = 'sva_monitoring_adapter'

logFormatter = logging.Formatter("%(asctime)s [%(threadName)-1.12s]  %(message)s")
logger = logging.getLogger()

if len(logger.handlers) == 0:
    project_path = os.path.dirname(os.path.realpath(__file__))
    fileHandler = logging.FileHandler("{0}/{1}.log".format(os.path.join(project_path), 'monitoring'))
    fileHandler.setFormatter(logFormatter)
    logger.addHandler(fileHandler)

    consoleHandler = logging.StreamHandler(sys.stdout)
    consoleHandler.setFormatter(logFormatter)
    logger.addHandler(consoleHandler)
    logger.setLevel(logging.INFO)


def get_file_age(filename):
    return os.path.getmtime(filename)


def send_event_report(component, obj, labels, types, value, event_hub=True):
    timestamp = datetime.datetime.now(pytz.utc)
    payload = {
        'component': component,
        'object': obj,
        'labels': labels,
        'type': types,
        'data': {
            'value': value
        },
        'timestamp': time.mktime(timestamp.timetuple())
    }
    json_payload = json.dumps(payload)
    settings_object = Settings.get()
    urls = [settings_object.event_hub_url, settings_object.django_url + settings.SVA_INFORMATION]
    if not event_hub:
        urls = urls[1:]
    sent = True
    for url in urls:
        status_code = send_post(url, data=json_payload)
        if status_code != 200:
            sent = False
    return sent


def run_in_periods(func):
    """
    :param func: function to run in background every n seconds, function is required to have frequency parameter
    :return: function itself, which executes additional stuff written in that function
    """
    def func_wrapper(self, frequency, delay=0, event_type=EVENT):
        if frequency > 0:
            time.sleep(delay)
            threading.Timer(frequency, func_wrapper, [self, frequency]).start()
        return func(self, frequency, delay, event_type)
    return func_wrapper


def send_availability(log, metric, file_available, event_type):
    logger.info(log % file_available)
    return send_event_report(SVA_MONITORING_ADAPTER, SVA_MONITORING_ADAPTER, [event_type], metric, file_available)


def send_age_report(log, metric, age, event_type):
    logger.info(log % age)
    return send_event_report(SVA_MONITORING_ADAPTER, SVA_MONITORING_ADAPTER, [event_type], metric, age)


class Config:
    def __init__(self, report_basic_age_frequency, list_availability_frequency, scanner_availability_frequency, list_age_frequency, repository_availability_frequency, up_report_age_frequency, scan_report_availability_frequency, up_report_availability_frequency, default_availability_delay, age_delay):

        self.report_basic_age_frequency = report_basic_age_frequency
        self.list_availability_frequency = list_availability_frequency
        self.scanner_availability_frequency = scanner_availability_frequency
        self.list_age_frequency = list_age_frequency
        self.repository_availability_frequency = repository_availability_frequency
        self.up_report_age_frequency = up_report_age_frequency
        self.scan_report_availability_frequency = scan_report_availability_frequency
        self.up_report_availability_frequency = up_report_availability_frequency

        self.age_delay = age_delay
        self.availability_delay = default_availability_delay

import sys
import os
import signal
import argparse
from basic_scan_rules import BasicScanRules
from list_update_rules import ListUpdateRules
from up_report_rules import UpgradeReportRules
from monitoringComponent import MonitoringComponent
from config import Config
from utils import logger
from constants import REMEDIATION_EVENT
from specs_sva_core.utils import init_database, set_dashboard_url, set_event_hub_url, \
    set_sla_id, set_component_id


class Monitoring:
    def __init__(self):
        parser = argparse.ArgumentParser(
            description='Monitors enforcement component',
        )

        parser.add_argument('command', help='Subcommand to run')
        args = parser.parse_args(sys.argv[1:2])
        if not hasattr(self, args.command):
            print 'Unrecognized command'
            parser.print_help()
            exit(1)

        init_database()
        self.basic_scan_rules = BasicScanRules()
        self.list_update_rules = ListUpdateRules()
        self.up_report_rules = UpgradeReportRules()
        getattr(self, args.command)()

    def run_monitoring(self):
        parser = argparse.ArgumentParser(
            description='Runs periodic monitoring'
        )
        parser.add_argument('report_basic_age_frequency', type=int, default=0)
        parser.add_argument('list_availability_frequency', type=int, default=0)
        parser.add_argument('scanner_availability_frequency', type=int, default=0)
        parser.add_argument('list_age_frequency', type=int, default=0)
        parser.add_argument('repository_availability_frequency', type=int, default=0)
        parser.add_argument('up_report_age_frequency', type=int, default=0)
        parser.add_argument('scan_report_availability_frequency', type=int, default=0)
        parser.add_argument('up_report_availability_frequency', type=int, default=0)
        parser.add_argument('availability_delay', type=int, default=7200)
        parser.add_argument('age_delay', type=int, default=3600)
        args = parser.parse_args(sys.argv[2:])

        # Saves PID to file, so we can kill it later
        pid = os.getpid()
        logger.info('Running SVA Monitoring on PID: %s' % pid)
        f = open('/tmp/monitoring.pid', 'w+')
        f.write('%s' % pid)
        f.close()

        self.list_update_rules.send_current_repository()
        config = Config(args.report_basic_age_frequency, args.list_availability_frequency,
                        args.scanner_availability_frequency,
                        args.list_age_frequency, args.repository_availability_frequency, args.up_report_age_frequency,
                        args.scan_report_availability_frequency,
                        args.up_report_availability_frequency, args.availability_delay, args.age_delay)

        monitoring = MonitoringComponent(config)
        monitoring.run()

    def set_dashboard_url(self):
        parser = argparse.ArgumentParser(
            description='Sets url of dashboard component'
        )
        parser.add_argument('url')
        args = parser.parse_args(sys.argv[2:])
        set_dashboard_url(args.url)

    def set_event_hub_url(self):
        parser = argparse.ArgumentParser(
            description='Sets url of event hub component'
        )
        parser.add_argument('url')
        args = parser.parse_args(sys.argv[2:])
        set_event_hub_url(args.url)

    def set_sla_id(self):
        parser = argparse.ArgumentParser(
            description='Sets SLA ID'
        )
        parser.add_argument('sla_id')
        args = parser.parse_args(sys.argv[2:])
        set_sla_id(args.sla_id)

    def set_component_id(self):
        parser = argparse.ArgumentParser(
            description='Sets Component ID'
        )
        parser.add_argument('component_id')
        args = parser.parse_args(sys.argv[2:])
        set_component_id(args.component_id)

    def invoke_msr5(self):
        # TODO
        pass

    def invoke_msr6(self):
        """
        Invokes repository availability measurement
        """

        self.list_update_rules.get_repositories_availability(0, event_type=REMEDIATION_EVENT)

    def invoke_msr7(self):
        """
        Invokes list availability measurement
        """

        self.basic_scan_rules.get_list_availability(0, event_type=REMEDIATION_EVENT)

    def invoke_msr8(self):
        """
        Invokes availability of installed scanners
        """

        self.basic_scan_rules.get_scanner_availability(0, event_type=REMEDIATION_EVENT)

    def invoke_msr9(self):
        """
        Invokes availability of scan report
        """

        self.up_report_rules.get_scan_report_availability(0, event_type=REMEDIATION_EVENT)

    def invoke_msr10(self):
        """
        Invokes availability of upgrade report
        """

        self.up_report_rules.get_up_report_availability(0, event_type=REMEDIATION_EVENT)


def main():
    Monitoring()


def signal_term_handler(signal, frame):
    logger.info('Exiting ...')
    sys.exit(0)


if __name__ == '__main__':
    signal.signal(signal.SIGTERM, signal_term_handler)
    main()

import time
import sys
from basic_scan_rules import BasicScanRules
from list_update_rules import ListUpdateRules
from up_report_rules import UpgradeReportRules
from utils import logger


class MonitoringComponent:
    def __init__(self, config):
        self.config = config
        self.monitoring_components = [BasicScanRules(), ListUpdateRules(), UpgradeReportRules()]

    def run(self):
        # Executes all measurements in background and then run them periodically
        for component in self.monitoring_components:
            component.run(self.config)

        while True:
            # We keep program running, so threads don't get killed, and wait for keyboard interrupt
            try:
                time.sleep(1)
            except KeyboardInterrupt:
                logger.info('Exiting ...')
                sys.exit(0)


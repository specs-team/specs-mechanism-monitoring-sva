import urllib
from threading import Thread
from utils import get_file_age, run_in_periods, send_event_report, logger
import os
from specs_sva_core.models import Settings
from specs_sva_core import settings
from constants import EVENT
import constants


class ListUpdateRules:
    def __init__(self):
        self.vulnerability_list = settings.VULNERABILITY_LIST
        self.repositories = settings.OVAL_REPOSITORIES
        self.sla_id = Settings.get().sla_id
        self.component_id = Settings.get().component_id

    def run(self, config):
        threads = [Thread(target=self.get_repositories_availability,
                          args=(config.repository_availability_frequency, config.availability_delay,)),
                   Thread(target=self.get_list_age,
                          args=(config.list_age_frequency, config.age_delay,)),
                   ]
        for thread in threads:
            thread.daemon = True
            thread.start()

    @run_in_periods
    def get_list_age(self, list_age_frequency, delay=0, event_type=EVENT):
        if list_age_frequency > 0:
            return self._send_list_age(event_type)
        return False

    @run_in_periods
    def get_repositories_availability(self, repository_availability_frequency, delay=0, event_type=EVENT):
        return self._send_repository_availability(event_type)

    def send_current_repository(self):
        self.send_report('info', 'current_repository', Settings.get().current_repository, event_hub=False)

    def _send_repository_availability(self, event_type):
        current_repository = Settings.get().current_repository
        is_online = urllib.urlopen(current_repository).getcode() == 200
        logger.info('Repository online: %s' % is_online)
        self.send_report('info', 'current_repository', Settings.get().current_repository, event_hub=False)
        return self.send_report(event_type, constants.REPOSITORY_AVAILABILITY, is_online)

    def _send_list_age(self, event_type):
        if os.path.isfile(self.vulnerability_list):
            age = get_file_age(self.vulnerability_list)
            logger.info('List age: %s' % age)
            return self.send_report(event_type, constants.LIST_AGE, age)
        return False

    def send_report(self, event_type, type, value, event_hub=True):
        return send_event_report(self.component_id, constants.LIST_UPDATE,
                                 [event_type, 'sla_id:%s' % self.sla_id, constants.LIST_UPDATE_METRIC], type, value,
                                 event_hub=event_hub)

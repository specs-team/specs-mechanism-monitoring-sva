from unittest import TestCase
from src.monitoring import main
import sys


def invoke(command):
    sys.argv = ['monitoring', command]
    main()


class TestMonitoring(TestCase):

    def tearDown(self):
        self.up_report = None

    def test_invoke_msr_6(self):
        try:
            invoke('invoke_msr6')
        except SyntaxError:
            self.fail("raised SyntaxError unexpectedly!")

    def test_invoke_msr_7(self):
        try:
            invoke('invoke_msr7')
        except SyntaxError:
            self.fail("raised SyntaxError unexpectedly!")

    def test_invoke_msr_8(self):
        try:
            invoke('invoke_msr8')
        except SyntaxError:
            self.fail("raised SyntaxError unexpectedly!")

    def test_invoke_msr_9(self):
        try:
            invoke('invoke_msr9')
        except SyntaxError:
            self.fail("raised SyntaxError unexpectedly!")

    def test_test_invoke_msr_10(self):
        try:
            invoke('invoke_msr10')
        except SyntaxError:
            self.fail("raised SyntaxError unexpectedly!")

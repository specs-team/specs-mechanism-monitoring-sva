from unittest import TestCase
import sys

import os

from src.up_report_rules import UpgradeReportRules

sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../'))
from specs_sva_core.utils import init_database, set_event_hub_url, set_dashboard_url, set_sla_id, set_component_id
import test_settings as settings


class TestUpgradeReportRules(TestCase):
    def setUp(self):
        init_database()

        set_event_hub_url(settings.EVENT_HUB_URL)
        set_dashboard_url(settings.DJANGO_SVA_INFORMATION)
        set_sla_id(settings.SLA_ID)
        set_component_id(settings.COMPONENT_ID)

        self.upgrade_report_rules = UpgradeReportRules()

    def tearDown(self):
        self.upgrade_report_rules = None

    def test_get_up_report_age_not_selected(self):
        sent = self.upgrade_report_rules.get_up_report_age(0)
        self.assertFalse(sent)

    def test_get_up_report_age_selected(self):
        filename = self.upgrade_report_rules.up_report
        if not os.path.exists(os.path.dirname(filename)):
            os.makedirs(os.path.dirname(filename))
        with open(filename, "w") as f:
            f.write("test")
        sent = self.upgrade_report_rules._send_up_report_age('event')
        self.assertTrue(sent)

    def test_get_scan_report_availability(self):
        sent = self.upgrade_report_rules.get_scan_report_availability(0)
        self.assertTrue(sent)

    def test_get_up_report_availability(self):
        sent = self.upgrade_report_rules.get_up_report_availability(0)
        self.assertTrue(sent)

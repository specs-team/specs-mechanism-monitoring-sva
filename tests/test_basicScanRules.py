from unittest import TestCase
import sys

import os

from src.basic_scan_rules import BasicScanRules

sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../'))
from specs_sva_core.utils import init_database, set_event_hub_url, set_dashboard_url, set_sla_id, set_component_id
import test_settings as settings


class TestBasicScanRules(TestCase):
    def setUp(self):
        init_database()

        set_event_hub_url(settings.EVENT_HUB_URL)
        set_dashboard_url(settings.DJANGO_SVA_INFORMATION)
        set_sla_id(settings.SLA_ID)
        set_component_id(settings.COMPONENT_ID)

        self.basic_scan_rules = BasicScanRules()

    def tearDown(self):
        self.basic_scan_rules = None

    def test_get_report_basic_age_not_selected(self):
        sent = self.basic_scan_rules.get_report_basic_age(0)
        self.assertFalse(sent)

    def test_get_report_basic_age_selected(self):
        filename = self.basic_scan_rules.scan_report
        if not os.path.exists(os.path.dirname(filename)):
            os.makedirs(os.path.dirname(filename))
        with open(filename, "w") as f:
            f.write("test")
        sent = self.basic_scan_rules._send_basic_age('event')
        self.assertTrue(sent)

    def test_get_list_availability(self):
        sent = self.basic_scan_rules.get_list_availability(0)
        self.assertTrue(sent)

    def test_get_scanner_availability(self):
        sent = self.basic_scan_rules.get_scanner_availability(0)
        self.assertTrue(sent)

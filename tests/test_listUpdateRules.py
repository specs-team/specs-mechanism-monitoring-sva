from unittest import TestCase
import sys

import os

from src.list_update_rules import ListUpdateRules

sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../'))
from specs_sva_core.utils import init_database, set_event_hub_url, set_dashboard_url, set_sla_id, set_component_id
import test_settings as settings


class TestListUpdateRules(TestCase):
    def setUp(self):
        init_database()

        set_event_hub_url(settings.EVENT_HUB_URL)
        set_dashboard_url(settings.DJANGO_SVA_INFORMATION)
        set_sla_id(settings.SLA_ID)
        set_component_id(settings.COMPONENT_ID)

        self.list_update_rules = ListUpdateRules()

    def tearDown(self):
        self.list_update_rules = None

    def test_get_list_age_not_selected(self):
        sent = self.list_update_rules.get_list_age(0)
        self.assertFalse(sent)

    def test_get_list_age_selected(self):
        filename = self.list_update_rules.vulnerability_list
        if not os.path.exists(os.path.dirname(filename)):
            os.makedirs(os.path.dirname(filename))
        with open(filename, "w") as f:
            f.write("test")
        sent = self.list_update_rules._send_list_age('event')
        self.assertTrue(sent)

    def test_get_repositories_availability(self):
        sent = self.list_update_rules.get_repositories_availability(0)
        self.assertTrue(sent)
